import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Alert, Typography } from 'antd';
import styles from './Welcome.less';

const CodePreview: React.FC<{}> = ({ children }) => (
  <pre className={styles.pre}>
    <code>
      <Typography.Text copyable>{children}</Typography.Text>
    </code>
  </pre>
);

export default (): React.ReactNode => (
  <PageContainer>
    <Card>
      <Alert
        message="让我们一起变的更强大。"
        type="success"
        showIcon
        banner
        style={{
          margin: -12,
          marginBottom: 24,
        }}
      />
      <Typography.Text strong>everyDay is a good day</Typography.Text>
      <CodePreview>只有飞速的旋转，才能止住我的泪水忘记你的模样</CodePreview>
      <Typography.Text
        strong
        style={{
          marginBottom: 12,
        }}
      >
        尽量哈
      </Typography.Text>
      <CodePreview>尽量切个分支 在自己路由下玩</CodePreview>
    </Card>
  </PageContainer>
);
