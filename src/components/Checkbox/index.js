import React, { Component } from 'react';
import { Checkbox } from 'antd';

const CheckboxGroup = Checkbox.Group;

export default class DistCheckbox extends Component {
  state = {
    indeterminate: true,
    isCheckedAll: false,
    options: [],
  };

  componentDidMount() {
    const { options = [] } = this.props;
    this.setState({
      options,
    });
  }

  onChange = (value) => {
    const { options } = this.state;
    this.setState({
      indeterminate: !!value.length && value.length < options.length,
      isCheckedAll: value.length === options.length,
    });
    this.save(value);
  };

  onCheckAllChange = (e) => {
    const { options } = this.state;
    console.log(e.target.checked, options, ' e.target.checked');
    const value = e.target.checked ? options : [];
    this.setState({
      indeterminate: false,
      isCheckedAll: e.target.checked,
    });
    this.save(value.map((item) => item.value || item));
  };

  save = (value) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(value);
    }
  };

  render() {
    const { isCheckedAll, indeterminate, options } = this.state;
    const { value } = this.props;
    return (
      <div>
        <div style={{ borderBottom: '1px solid #E9E9E9' }}>
          <Checkbox
            indeterminate={indeterminate}
            onChange={this.onCheckAllChange}
            checked={isCheckedAll}
          >
            全选
          </Checkbox>
        </div>
        <br />
        <CheckboxGroup options={options} value={value} onChange={this.onChange} />
      </div>
    );
  }
}
