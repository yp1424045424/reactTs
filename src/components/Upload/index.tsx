import React, { useEffect, useState } from 'react';
import { Input, InputNumber } from 'antd';

const UploadImgs: React.FC<{}> = ({ onChange, value }) => {
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    console.log(value);
    setInputValue(value);
  }, [value]);

  // useEffect(()=>{
  //   console.log(value)
  //   onChange(inputValue)
  // },[inputValue])

  const handleInputChange = (e) => {
    console.log(e.target.value);
    setInputValue(e.target.value);
    onChange(e.target.value);
  };

  return (
    <div>
      <Input onChange={handleInputChange} value={inputValue} />
      Input框输入的字节长度 <InputNumber value={inputValue?.length} /> （此内容不提交）
    </div>
  );
};
export default UploadImgs;
