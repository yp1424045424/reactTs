export default {
  publicPath: '/',
  define: {
    'process.env.APP_MODE': 'development',
    'process.env.FRONT_BASE_URL': 'http://localhost:8000/#/',
    'process.env.API_BASE_URL': 'http://test.com/deerhorn',
  },
};
