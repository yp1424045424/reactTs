// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const { REACT_APP_ENV } = process.env;

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  layout: {
    name: '杨鹏侧边栏',
    locale: false,
    siderWidth: 208,
  },
  locale: {
    // default zh-CN
    // default true, when it is true, will use `navigator.language` overwrite default
    antd: true,
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },

  routes: [
    {
      path: '/welcome',
      name: '欢迎来到凸凹实验室',
      icon: 'smile',
      component: './Welcome',
    },
    {
      path: '/admin',
      name: 'ypp的创作空间',
      icon: 'crown',
      access: 'canAdmin',
      component: './TestForm',
      routes: [
        {
          path: '/admin/sub-page',
          name: 'form-demo',
          icon: 'smile',
          component: './TestForm',
        },
      ],
    },
    {
      name: '第一个吃螃蟹的人',
      icon: 'table',
      path: '/list',
      component: './ListTableList',
    },
    {
      path: '/',
      redirect: '/welcome',
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
});
